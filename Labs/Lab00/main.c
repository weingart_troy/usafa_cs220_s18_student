/** main.c
* ===========================================================
* Name: Troy Weingart, 21 Dec 17
* Section: n/a
* Project: Sample file
* Purpose: Hello world!
* ===========================================================
*/
#include <stdio.h>

int main() {
    printf("Hello, World!\n");
    return 0;
}